import logging
import threading
import queue
import traceback
import binascii
import struct
import time
from bluepy.btle import UUID, DefaultDelegate, Peripheral

import voluptuous as vol
import homeassistant.util.dt as dt_util

import homeassistant.helpers.config_validation as cv
from homeassistant.helpers.entity import Entity
from homeassistant.components.sensor import PLATFORM_SCHEMA
from homeassistant.const import (
    CONF_NAME, CONF_MAC, TEMP_CELSIUS, STATE_UNKNOWN, EVENT_STATE_CHANGED, EVENT_HOMEASSISTANT_STOP)

REQUIREMENTS	=	[]

_LOGGER			=	logging.getLogger(__name__)

CONNECT_LOCK	=	threading.Lock()
DATAPACKET_LOCK	=	threading.Lock()

DEVICE_TAG		=	"ASTHMADEVICE"

CONF_DEVICE_ID	=	"device_id"
CONF_DEF_ADAP	=	"hci0"
ATTR_DEVICE		=	'device'
ATTR_MODEL		=	'model'
DATA_UUID		=	UUID(0xFFF4)

PLATFORM_SCHEMA = PLATFORM_SCHEMA.extend({
    vol.Required(CONF_MAC): cv.string,
    vol.Optional(CONF_NAME, default=""): cv.string,
    vol.Optional(CONF_DEVICE_ID,default=CONF_DEF_ADAP): cv.string, 
})

#fff0 is the service uuid. 
#fff3 is used for sending command packets to the device.
#fff4 is used to receive data packets from the device.

ASTHMA_SERVICE_UUID	=	'0000fff0-0000-1000-8000-00805f9b34fb'
BLE_COMMAND_UUID	=	'0000fff3-0000-1000-8000-00805f9b34fb'
BLE_DATA_UUID		=	'0000fff4-0000-1000-8000-00805f9b34fb'

BLE_DATA_HANDLE		=	0x0027
SKIP_HANDLE_LOOKUP	=	True
HAS_HASS_BUS_STOPPED = 	False	#Flag to end BLE thread when HASS bus has stopped
TIMEOUT_24HR		=	86400
TIMEOUT_12HR		=	43200
TIMEOUT_01HR		=	3600
TIMEOUT_01MN		=	60
CONNECT_TIMEOUT		=	TIMEOUT_01MN	#sleep time in seconds before re-trying connection to the device
TIMEOUT_LOCK		=	queue.Queue()

BYTE_SCALER			=	8

#define necessary baseline and length of packets

command_packet_length	=	10
data_packet_length		=	30
flash_start_addr		=	65536
flash_packet_size		=	17
flash_sector_size		=	0x1000


#start bytes and length are used to verify the goodness of the packet
realtime_command_start_byte1	=	0x01
realtime_command_start_byte2	=	0x01

flash_command_start_byte1	=	0x02
flash_command_start_byte2	=	0x02

flash_data_start_byte1		=	0x0D
flash_data_start_byte2		=	0x17

flash_start_start_byte1		=	0x11
flash_start_start_byte2		=	0x11

flash_end_start_byte1		=	0x22
flash_end_start_byte2		=	0x22

#define the indices of various variables in the packets

erase_flag_index			=	[2]
command_address_index		=	[5,4,3]
command_no_of_packets_index	=	[9,8,7,6]

header_device_type_index	=	[2]
header_device_index			= 	[4,3]
header_total_packets_index	= 	[7,6,5]
header_flash_start_addr_index	= 	[10,9,8]


start_byte1_index		=	[0]
start_byte2_index		=	[1]
packet_index_index		=	[4,3,2]
timestamp_index			=	[8,7,6,5]
o3_index				=	[10,9]
no2_index				=	[12,11]
voc_index				=	[14,13]
accelerometer_index		=	[18]
battery_index			=	[19]
temperature_index		=	[20]
humidity_index			=	[21]

# Sensor Names, Units, Indices
temperature_sensor_name	=	"Temperature"
humidity_sensor_name	=	"Humidity"
ozone_sensor_name		=	"Ozone"
no2_sensor_name			=	"NO2"
voc_sensor_name			=	"VOC"
battery_sensor_name		=	"Battery"
accelerometer_sensor_name	=	"Accelerometer"


temperature_unit	=	TEMP_CELSIUS
humidity_unit		=	"%RH"
ozone_unit			=	"ppb"
no2_unit			=	"ppb"
voc_unit			=	"ppm"
battery_unit		=	"%"
accelerometer_unit	=	"g"


temperature_sensor_index	=	0
humidity_sensor_index		=	1
ozone_sensor_index			=	2
no2_sensor_index			=	3
voc_sensor_index			=	4
battery_sensor_index		=	5
accelerometer_sensor_index	=	6

sensor_units = [temperature_unit, humidity_unit, ozone_unit, no2_unit, voc_unit, battery_unit, accelerometer_unit]

mon = None

# pylint: disable=unused-argument
def setup_platform(hass, config, add_devices, discovery_info=None):
	"""Set up the sensor."""
	name	=	config.get(CONF_NAME)
	mac	=	config.get(CONF_MAC)
	_LOGGER.debug("Setting up...")
	global mon
	mon	=	Monitor(hass, mac, name)
    
	#Define new entity for each sensor whose data needs to be captured
	asthmasensortemperature		=	AsthmaDevice(temperature_sensor_name, temperature_sensor_index, mon.temperature_update_list, mon)
	asthmasensorhumidity		=	AsthmaDevice(humidity_sensor_name, humidity_sensor_index, mon.humidity_update_list, mon)
	asthmasensorozone			=	AsthmaDevice(ozone_sensor_name, ozone_sensor_index, mon.ozone_update_list, mon)
	asthmasensorno2				=	AsthmaDevice(no2_sensor_name, no2_sensor_index, mon.no2_update_list, mon)
	asthmasensorvoc				=	AsthmaDevice(voc_sensor_name, voc_sensor_index, mon.voc_update_list, mon)
	asthmasensorbattery			=	AsthmaDevice(battery_sensor_name, battery_sensor_index, mon.battery_update_list, mon)
	asthmasensoraccelerometer	=	AsthmaDevice(accelerometer_sensor_name, accelerometer_sensor_index, mon.accelerometer_update_list, mon)

	mon.asthma_sensors.append(asthmasensortemperature)
	mon.asthma_sensors.append(asthmasensorhumidity)
	mon.asthma_sensors.append(asthmasensorozone)
	mon.asthma_sensors.append(asthmasensorno2)
	mon.asthma_sensors.append(asthmasensorvoc)
	mon.asthma_sensors.append(asthmasensorbattery)
	mon.asthma_sensors.append(asthmasensoraccelerometer)

	mon.update_dev_states_temperature	=	asthmasensortemperature.update
	mon.update_dev_states_humidity		=	asthmasensorhumidity.update
	mon.update_dev_states_ozone			=	asthmasensorozone.update
	mon.update_dev_states_no2			=	asthmasensorno2.update
	mon.update_dev_states_voc			=	asthmasensorvoc.update
	mon.update_dev_states_battery		=	asthmasensorbattery.update
	mon.update_dev_states_accelerometer	=	asthmasensoraccelerometer.update

	add_devices(mon.asthma_sensors)

	def monitor_stop(_service_or_event):
		"""Stop the monitor thread."""
		_LOGGER.debug("Stopping monitor for %s", name)
		global HAS_HASS_BUS_STOPPED
		global CONNECT_TIMEOUT
		global TIMEOUT_LOCK
		HAS_HASS_BUS_STOPPED = True
		TIMEOUT_LOCK.put(1)
		CONNECT_TIMEOUT = TIMEOUT_01MN
		mon.terminate()

	def monitor_state_update(event):
		"streamline sensor value update"
		new_state	= event.data.get("new_state")
		#device_name = new_state.attributes.get("device")
		sensor_name = event.data.get("new_state").name
		if(sensor_name is not None):
			update_list = mon.dict_sensorupdatelist.get(sensor_name,None)
			if update_list is not None and len(update_list) > 0:
				updated_packet = update_list.pop(0)
				_LOGGER.debug("%s : %ld", sensor_name, updated_packet.getPacketIndex())
			
#.attributes.get("device"))

	hass.bus.listen(EVENT_STATE_CHANGED, monitor_state_update)
	hass.bus.listen_once(EVENT_HOMEASSISTANT_STOP, monitor_stop)
	mon.start()


#define the asthma device sensors with their respective units
class AsthmaDevice(Entity):
	"""Representation of the accelerometer sensor."""

	def __init__(self, name, index, dataqueue, mon):
		"""Initialize a sensor."""
		self.mon	= mon
		self._name	= name
		self._state	= None
		self._index = index
		self._unit	= sensor_units[self._index]
		self._dataqueue = dataqueue

	@property
	def name(self):
		"""Return the name of the sensor."""
		return self._name

	@property
	def state(self):
		"""Return the state of the device."""
		return self._state

	@property
	def unit_of_measurement(self):
		"""Return the unit the value is expressed in."""
		return self._unit

	@property
	def device_state_attributes(self):
		"""Return the state attributes of the sensor."""
		timestamp_value = 0
		if len(self._dataqueue) > 0:
			timestamp_value = self._dataqueue[0].getTimeStamp()
		return {
			ATTR_DEVICE: DEVICE_TAG,
			ATTR_MODEL: 1,
			'sample_time' : dt_util.utc_from_timestamp(timestamp_value),
		}

	def update(self):
		if len(self._dataqueue) > 0:
			self._state = self._dataqueue[0].getElementByIndex(self._index)
			self.schedule_update_ha_state()

	@property
	def force_update(self):
		return True


#define the start packet for the protocol which defines the total number of packets and baseline for the current session
class AsthmaStartPacket:
	
	device_type			=	0
	device_index		=	0
	total_packets		=	0
	flash_start_address	=	0
	flash_high_address	=	0
	count_index			=	0

	def __init__(self, dataarray=[]):
		self.inputarray = dataarray
		if len(self.inputarray) == data_packet_length:
			for i in range(0,len(header_device_type_index)):
				self.device_type	+=	(self.inputarray[header_device_type_index[i]] << (BYTE_SCALER * i))

			for i in range(0,len(header_device_index)):
				self.device_index	+=	(self.inputarray[header_device_index[i]] << (BYTE_SCALER * i))

			for i in range(0,len(header_total_packets_index)):
				#_LOGGER.debug("index is %s and header indices are %s",i,header_total_packets_index[i])
				self.total_packets	+=	(self.inputarray[header_total_packets_index[i]] << (BYTE_SCALER * i))

			for i in range(0,len(header_flash_start_addr_index)):
				self.flash_start_address	+=	(self.inputarray[header_flash_start_addr_index[i]] << (BYTE_SCALER * i))
			self.flash_high_address = self.flash_start_address + (flash_packet_size * self.total_packets)
			if self.flash_start_address >= flash_start_addr:
				self.count_index	=	((self.flash_start_address - flash_start_addr)/flash_packet_size) + 1
		else:
			self.device_type			=	0
			self.device_index			=	0
			self.total_packets			=	0
			self.flash_start_address	=	0
			self.flash_high_address 	=	0
			self.count_index			=	0

	def getDeviceType(self):
		return self.device_type
	def getDeviceIndex(self):
		return self.device_index
	def getTotalPackets(self):
		return self.total_packets
	def getFlashStartAddress(self):
		return self.flash_start_address
	def getFlashHighAddress(self):
		return self.flash_high_address
	def getStartCount(self):
		return self.count_index


#define DataPacket which contains data of the sensors from the current session
class AsthmaDataPacket:
	start_byte1			=	0
	start_byte2			=	0
	packet_index		=	0
	count_index			=	0
	packet_timestamp	=	0
	o3_conc				=	0.0
	no2_conc			=	0.0
	voc_conc			=	0.0
	accelerometer		=	0.0
	battery_level		=	0.0
	temperature			=	0.0
	humidity			=	0.0
	dict_sensormap		=	{}
	
	def __init__(self, dataarray=[]):
		self.inputarray = dataarray
		if len(self.inputarray) == data_packet_length:
			for i in range(0,len(start_byte1_index)):
				self.start_byte1	+=	(self.inputarray[start_byte1_index[i]] << (BYTE_SCALER * i))

			for i in range(0,len(start_byte2_index)):
				self.start_byte2	+=	(self.inputarray[start_byte2_index[i]] << (BYTE_SCALER * i))

			for i in range(0,len(packet_index_index)):
				self.packet_index	+=	(self.inputarray[packet_index_index[i]] << (BYTE_SCALER * i))
			if self.packet_index >= flash_start_addr:
				self.count_index	=	((self.packet_index - flash_start_addr)/flash_packet_size) + 1

			for i in range(0,len(timestamp_index)):
				self.packet_timestamp	+=	(self.inputarray[timestamp_index[i]] << (BYTE_SCALER * i))
			#_LOGGER.debug("timestamp: %lu",self.packet_timestamp)

			for i in range(0,len(o3_index)):
				self.o3_conc	+=	(self.inputarray[o3_index[i]] << (BYTE_SCALER * i))

			for i in range(0,len(no2_index)):
				self.no2_conc	+=	(self.inputarray[no2_index[i]] << (BYTE_SCALER * i))
			self.no2_conc = 0

			for i in range(0,len(voc_index)):
				self.voc_conc	+=	(self.inputarray[voc_index[i]] << (BYTE_SCALER * i))

			for i in range(0,len(accelerometer_index)):
				self.accelerometer	+=	(self.inputarray[accelerometer_index[i]] << (BYTE_SCALER * i))

			for i in range(0,len(battery_index)):
				self.battery_level	+=	(self.inputarray[battery_index[i]] << (BYTE_SCALER * i))

			for i in range(0,len(temperature_index)):
				self.temperature	+=	(self.inputarray[temperature_index[i]] << (BYTE_SCALER * i))

			for i in range(0,len(humidity_index)):
				self.humidity	+=	(self.inputarray[humidity_index[i]] << (BYTE_SCALER * i))

		else:
			self.start_byte1	=	0
			self.start_byte2	=	0
			self.packet_index	=	0
			self.count_index	=	0
			self.packet_timestamp	=	0
			self.o3_conc		=	0
			self.no2_conc		=	0
			self.voc_conc		=	0
			self.accelerometer	=	0
			self.battery_level	=	0
			self.temperature	=	0
			self.humidity		=	0

		self.dict_sensormap[ozone_sensor_index] = self.o3_conc
		self.dict_sensormap[no2_sensor_index] = self.no2_conc
		self.dict_sensormap[voc_sensor_index] = self.voc_conc
		self.dict_sensormap[accelerometer_sensor_index] = self.accelerometer
		self.dict_sensormap[battery_sensor_index] = self.battery_level
		self.dict_sensormap[temperature_sensor_index] = self.temperature
		self.dict_sensormap[humidity_sensor_index] = self.humidity
		

	def getStartByte1(self):
		return self.start_byte1
	def getStartByte2(self):
		return self.start_byte2
	def getPacketIndex(self):
		return self.packet_index
	def getPacketCount(self):
		return self.count_index
	def getTimeStamp(self):
		return self.packet_timestamp
	def getO3Conc(self):
		return self.o3_conc
	def getNO2Conc(self):
		return self.no2_conc
	def getVOCConc(self):
		return self.voc_conc
	def getAccelerometer(self):
		return self.accelerometer
	def getBatteryLevel(self):
		return self.battery_level
	def getTemperature(self):
		return self.temperature
	def getHumidity(self):
		return self.humidity
	def getElementByIndex(self,index):
		return self.dict_sensormap.get(index,0)
			
				
#define CommandPacket for the protocol, which defines the next action of the device
class AsthmaCommandPacket:

	commandbytearray = bytearray(command_packet_length)
	def __init__(self,type, erase, startaddr, numberpackets):
		self.type = type
		self.erase = erase
		self.startaddr = startaddr
		self.numberpackets = int(numberpackets)
		
		if self.type == 0:
			for i in range(0,len(start_byte1_index)):
				self.commandbytearray[start_byte1_index[i]]	=	(flash_command_start_byte1 >> (BYTE_SCALER * i))
			for i in range(0,len(start_byte2_index)):
				self.commandbytearray[start_byte2_index[i]]	=	(flash_command_start_byte2 >> (BYTE_SCALER * i))
			for i in range(0,len(erase_flag_index)):
				self.commandbytearray[erase_flag_index[i]]	=	(self.erase >> (BYTE_SCALER * i))
			for i in range(0,len(command_address_index)):
				#_LOGGER.debug("command address index is %d",i)
				self.commandbytearray[command_address_index[i]]	=	(self.startaddr >> (BYTE_SCALER * i)) & 0xff
			for i in range(0,len(command_no_of_packets_index)):
				self.commandbytearray[command_no_of_packets_index[i]]	=	(self.numberpackets >> (BYTE_SCALER * i)) & 0xff
		elif self.type == 1:
			for i in range(0,len(start_byte1_index)):
				self.commandbytearray[start_byte1_index[i]]	=	(realtime_command_start_byte1 >> (BYTE_SCALER * i))
			for i in range(0,len(start_byte2_index)):
				self.commandbytearray[start_byte2_index[i]]	=	(realtime_command_start_byte2 >> (BYTE_SCALER * i))

	def getPacketBytes(self):
		return bytes(self.commandbytearray)


#define LostPackets is a list of <baseaddress, number of packets> 
#which are all the packets lost during the ble transmission
class AsthmaLostPackets:

	def __init__(self, baseAddress, NumberOfPacks):
		self.listBaseAddr	=	baseAddress
		self.listNoOfPackets	=	NumberOfPacks
		
	def getBaseAddress(self):
		return self.listBaseAddr
	def getNoOfPackets(self):
		return self.listNoOfPackets


#callback for new data received over the data characteristic
class AsthmaProtocol(DefaultDelegate):
	def __init__(self,name,mon):
		DefaultDelegate.__init__(self)
		self.name	=	name
		self.mon	=	mon
	def handleNotification(self, cHandle, value):
		#_LOGGER.debug("%s",value)

		with DATAPACKET_LOCK:
			#check for the goodness of the current data packet
			#check for the kind of packet received
			#Data Packet if successful
			if len(value) == data_packet_length and value[0] == flash_data_start_byte1 and value[1] == flash_data_start_byte2:
				currentDataPacket	=	AsthmaDataPacket(value)
				if self.mon.currentSessionStartPacket is not None and currentDataPacket.getPacketIndex() >= self.mon.currentSessionStartPacket.getFlashStartAddress() and currentDataPacket.getPacketIndex() <= self.mon.currentSessionStartPacket.getFlashHighAddress():

					self.mon.temperature_update_list.append(currentDataPacket)
					self.mon.update_dev_states_temperature()
					#_LOGGER.debug("Temperature: PacketIndex is %ld, PacketTimestamp is %ld", (currentDataPacket.getPacketCount()), currentDataPacket.getTimeStamp())

					self.mon.humidity_update_list.append(currentDataPacket)
					self.mon.update_dev_states_humidity()
					#_LOGGER.debug("Humidity: PacketIndex is %ld, PacketTimestamp is %ld", (currentDataPacket.getPacketCount()), currentDataPacket.getTimeStamp())

					self.mon.ozone_update_list.append(currentDataPacket)
					self.mon.update_dev_states_ozone()
					#_LOGGER.debug("Ozone: PacketIndex is %ld, PacketTimestamp is %ld", (currentDataPacket.getPacketCount()), currentDataPacket.getTimeStamp())

					self.mon.no2_update_list.append(currentDataPacket)
					self.mon.update_dev_states_no2()
					#_LOGGER.debug("NO2: PacketIndex is %ld, PacketTimestamp is %ld", (currentDataPacket.getPacketCount()), currentDataPacket.getTimeStamp())

					self.mon.voc_update_list.append(currentDataPacket)
					self.mon.update_dev_states_voc()
					#_LOGGER.debug("VOC: PacketIndex is %ld, PacketTimestamp is %ld", (currentDataPacket.getPacketCount()), currentDataPacket.getTimeStamp())

					self.mon.battery_update_list.append(currentDataPacket)
					self.mon.update_dev_states_battery()
					#_LOGGER.debug("Battery: PacketIndex is %ld, PacketTimestamp is %ld", (currentDataPacket.getPacketCount()), currentDataPacket.getTimeStamp())

					self.mon.accelerometer_update_list.append(currentDataPacket)
					self.mon.update_dev_states_accelerometer()
					#_LOGGER.debug("Accelerometer: PacketIndex is %ld, PacketTimestamp is %ld", (currentDataPacket.getPacketCount()), currentDataPacket.getTimeStamp())


					if self.mon.currentSessionLastReceivedDataPacket is not None:
						if self.mon.currentSessionLastReceivedDataPacket.getPacketIndex() >= self.mon.currentSessionStartPacket.getFlashStartAddress():
							packetDiffCount = currentDataPacket.getPacketCount() - self.mon.currentSessionLastReceivedDataPacket.getPacketCount()
							if packetDiffCount > 1:
								self.mon.lostPacketsList.append(AsthmaLostPackets(self.mon.currentSessionLastReceivedDataPacket.getPacketIndex() + flash_packet_size, packetDiffCount-1))
								#_LOGGER.debug("lost packets end index: %ld  start index: %ld",currentDataPacket.getPacketCount(),self.mon.currentSessionLastReceivedDataPacket.getPacketCount())
					self.mon.currentSessionLastReceivedDataPacket	=	currentDataPacket
					self.mon.allSessionsPacketCount			+=	1
			#Start Packet if successful
			elif len(value) == data_packet_length and value[0] == flash_start_start_byte1 and value[1] == flash_start_start_byte2:
				currentStartPacket	=	AsthmaStartPacket(value)
				self.mon.currentSessionStartPacket	=	currentStartPacket
				_LOGGER.debug("Number of packets requested are %ld, with base index: %ld",currentStartPacket.getTotalPackets(), currentStartPacket.getStartCount())
				if currentStartPacket.getFlashStartAddress() == flash_start_addr:
					self.mon.totalNumberOfPackets	=	currentStartPacket.getTotalPackets()
			#End Packet if successful
			elif len(value) == data_packet_length and value[0] == flash_end_start_byte1 and value[1] == flash_end_start_byte2:
				currentEndPacket	=	AsthmaStartPacket(value)
				_LOGGER.debug("Number of packets requested are %ld, with base index: %ld",currentEndPacket.getTotalPackets(), currentEndPacket.getStartCount())

			else:
				_LOGGER.debug("Invalid Packet\n")

		

		

class Monitor(threading.Thread):
	"""Connection handling."""

	def __init__(self, hass, mac, name):
		"""Construct interface object."""
		threading.Thread.__init__(self)
		self.daemon	=	False
		self.hass	=	hass
		self.mac	=	mac
		self.name	=	name

		self.dict_sensorupdatelist			=	{}
		self.temperature_update_list		=	[]
		self.humidity_update_list			=	[]
		self.ozone_update_list				=	[]
		self.no2_update_list				=	[]
		self.voc_update_list				=	[]
		self.battery_update_list			=	[]
		self.accelerometer_update_list		=	[]

		self.update_dev_states_temperature	=	None
		self.update_dev_states_humidity		=	None
		self.update_dev_states_ozone		=	None
		self.update_dev_states_no2			=	None
		self.update_dev_states_voc			=	None
		self.update_dev_states_accelerometer	=	None
		self.update_dev_states_battery		=	None

		self.connection_status				=	'disconnected'
		self.missing_packet_list			=	[]
		self.asthma_sensors					=	[]
		self.keep_going						=	True
		self.event							=	threading.Event()

		self.dict_sensorupdatelist[temperature_sensor_name]	=	self.temperature_update_list
		self.dict_sensorupdatelist[humidity_sensor_name]	=	self.humidity_update_list
		self.dict_sensorupdatelist[ozone_sensor_name]		=	self.ozone_update_list
		self.dict_sensorupdatelist[no2_sensor_name]			=	self.no2_update_list
		self.dict_sensorupdatelist[voc_sensor_name]			=	self.voc_update_list
		self.dict_sensorupdatelist[battery_sensor_name]		=	self.battery_update_list
		self.dict_sensorupdatelist[accelerometer_sensor_name]	=	self.accelerometer_update_list


	def run(self):
		"""Thread that keeps connection alive."""
		from bluepy.btle import Peripheral, BTLEException
		global HAS_HASS_BUS_STOPPED
		global CONNECT_TIMEOUT
		global TIMEOUT_LOCK
		while not HAS_HASS_BUS_STOPPED:
			try:
				with CONNECT_LOCK:
					_LOGGER.debug("Connecting to %s", self.name)
					asthmaperipheral = Peripheral(self.mac,"public",CONF_DEVICE_ID)
				asthmaperipheral.setMTU(50)
				global mon
				asthmaperipheral.setDelegate(AsthmaProtocol(self.name,mon))
				asthmaservices = asthmaperipheral.getServiceByUUID(ASTHMA_SERVICE_UUID)
				asthmadatacharac = asthmaservices.getCharacteristics(BLE_DATA_UUID)[0]
				_LOGGER.debug("data uuid is %s",asthmadatacharac.uuid)
				asthmacommandcharac = asthmaservices.getCharacteristics(BLE_COMMAND_UUID)[0]
				_LOGGER.debug("command uuid is %s", asthmacommandcharac.uuid)
				_LOGGER.debug("command handle is %s",asthmacommandcharac.valHandle)

				#generate command packet for real-time data
				#real_time_command = AsthmaCommandPacket(1,0,0,0)
				#_LOGGER.debug("asthma command packet is %s",*real_time_command.getPacketBytes())
				#command_packet = struct.pack('<bbbbbbbbbb',*real_time_command.getPacketBytes())

				mon.totalNumberOfPackets	=	0
				mon.allSessionsPacketCount	=	0
				mon.currentSessionLastReceivedDataPacket	=	None
				mon.currentSessionStartPacket			=	None
				mon.lostPacketsList				=	[]

				#Flash Data Command (type, erase flag, start address, number of packets)
				flash_command	=	AsthmaCommandPacket(0,0,flash_start_addr,0)
				#_LOGGER.debug("flash command packet is %s",flash_command.getPacketBytes())
				#Command packet for initializing Flash Data Transfer
				command_packet	=	struct.pack('<BBBBBBBBBB',*flash_command.getPacketBytes())
				#Activate notifications on FFF4
				asthmaperipheral.writeCharacteristic(asthmadatacharac.valHandle+1, struct.pack('<bb', 0x01, 0x00), False)
				#Send command packet
				asthmaperipheral.writeCharacteristic(asthmacommandcharac.valHandle,command_packet,False)

				while not HAS_HASS_BUS_STOPPED:
					if asthmaperipheral.waitForNotifications(2.0):
						continue
					if mon.totalNumberOfPackets <= mon.allSessionsPacketCount:
						#Transfer Protocol complete & successful
						#Send erase command
						#disconnect peripheral
						_LOGGER.debug("total number of packets: %ld received",mon.totalNumberOfPackets)
						erase_command		=	AsthmaCommandPacket(0,1,flash_start_addr,0)
						erase_command_packet	=	struct.pack('<BBBBBBBBBB',*erase_command.getPacketBytes())
						asthmaperipheral.writeCharacteristic(asthmacommandcharac.valHandle, erase_command_packet,False)
						asthmaperipheral.disconnect()
						break;
					
					#Get the packet index difference. This determines if packets have been lost and have to be stored to retreive again later
					packetDiffCount = ((mon.currentSessionStartPacket.getFlashHighAddress() - mon.currentSessionLastReceivedDataPacket.getPacketIndex())/flash_packet_size)
					if (packetDiffCount) > 1:
						_LOGGER.debug("lost packets no of packets: %ld  start index: %ld",packetDiffCount-1,mon.currentSessionLastReceivedDataPacket.getPacketCount()+1)
						mon.lostPacketsList.append(AsthmaLostPackets(mon.currentSessionLastReceivedDataPacket.getPacketIndex() + flash_packet_size, packetDiffCount-1))
					
					#Incase of any lost packets, re-request them until all the required packets are received
					if len(mon.lostPacketsList) > 0:
						lastLostPackets		=	mon.lostPacketsList.pop(0)
						#Flash Data Command (type, erase flag, start address, number of packets)
						#_LOGGER.debug("base address is %ld and no of packets are %ld",lastLostPackets.getBaseAddress(),lastLostPackets.getNoOfPackets())
						flash_command		=	AsthmaCommandPacket(0,0,lastLostPackets.getBaseAddress(),lastLostPackets.getNoOfPackets())
						#_LOGGER.debug("flash command packet is %s",flash_command.getPacketBytes())
						#Command packet for initializing Flash Data Transfer
						command_packet		=	struct.pack('<BBBBBBBBBB',*flash_command.getPacketBytes())
						#Send command packet
						asthmaperipheral.writeCharacteristic(asthmacommandcharac.valHandle,command_packet,False)
					#_LOGGER.debug("Connecting to %s", self.name)
				asthmaperipheral.disconnect()
				CONNECT_TIMEOUT = TIMEOUT_12HR
			except BTLEException:
				CONNECT_TIMEOUT = TIMEOUT_01HR
				_LOGGER.debug("Connection to %s failed. Re-trying connection in %d Hours, %d Minutes, %d Seconds", self.name, int(CONNECT_TIMEOUT/TIMEOUT_01HR), int((CONNECT_TIMEOUT%TIMEOUT_01HR)/TIMEOUT_01MN), int((CONNECT_TIMEOUT%TIMEOUT_01HR)%TIMEOUT_01MN))
			except:
				CONNECT_TIMEOUT = TIMEOUT_01HR
				_LOGGER.debug("Unknown Error Occured. Exiting...")
				traceback.print_exc()
			finally:
				try:
					_LOGGER.debug("Waiting...")
					ingore_dequeue = TIMEOUT_LOCK.get(True, CONNECT_TIMEOUT)
				except queue.Empty:
					_LOGGER.debug("Re-trying...")

	def terminate(self):
		"""Signal runner to stop and join thread."""
		self.keep_going = False
		self.event.set()
		self.join
