/*********************************************/

Purpose: Build sensor component for homeassistant to work with the Asthma Wearable device via BLE


Dependencies:

The component uses 'bluepy' library. You can install it using the following command

```
sudo pip install bluepy
```

Configuration:

```
sensor:
 - platform: bluepyasthma			/*Name of the sensor component '.py'*/
   mac: 'B0:B4:48:DD:62:3C'			/*Mac address of the Asthma Device*/
   device_id: hci0				/*BLE interface on the host machine*/
   name: 'Asthma V3-H'				/*BLE broadcast name of the Asthma Device*/
   monitored_conditions:			/*Sensors being used in the device*/
    - temperature
    - humidity
    - ozone
    - no2
    - voc
    - accelerometer
    - battery
```

Upcoming:

```
Replace individual Sensor initialization with configuration.yaml specific initialization
```

/*********************************************/
